package jsonstream

import (
	"encoding/json"
	"errors"
	"io"
)

var (
	ErrReceivedNilError       = errors.New("jsonstream: Received an nil error")
	ErrReceivedDataAfterError = errors.New("jsonstream: Received data after error")
)

func Write(w io.Writer, dataChan chan interface{}) error {
	if _, err := io.WriteString(w, "{"); err != nil {
		return err
	}
	datarec := false
	errorrec := false
	for v := range dataChan {
		//time.Sleep(1 * time.Second)
		if errData, ok := v.(error); ok {
			if errData == nil {
				return ErrReceivedNilError
			}
			if !errorrec {
				if datarec {
					if _, err := io.WriteString(w, "],"); err != nil {
						return err
					}
				}
				if _, err := io.WriteString(w, "\"error\":["); err != nil {
					return err
				}
				errorrec = true
			} else {
				if _, err := io.WriteString(w, ","); err != nil {
					return err
				}
			}

			s, err := json.Marshal(errData.Error())
			if err != nil {
				return err
			}
			if _, err := w.Write(s); err != nil {
				return err
			}
			continue
		}

		if errorrec {
			return ErrReceivedDataAfterError
		}

		//First Data received. Open result.
		if !datarec {
			if _, err := io.WriteString(w, "\"result\":["); err != nil {
				return err
			}
			datarec = true
		} else {
			w.Write([]byte(","))
		}
		s, err := json.Marshal(v)
		if err != nil {
			return err
		}
		if _, err = w.Write(s); err != nil {
			return err
		}
	}
	if datarec || errorrec {
		if _, err := io.WriteString(w, "]"); err != nil {
			return err
		}
	}
	if _, err := io.WriteString(w, "}"); err != nil {
		return err
	}
	return nil
}
